using System.Collections.Generic;


namespace Valhalla.Serialization.Extensions
{
	// ReSharper disable once InconsistentNaming
	public static class ISetExtensions
	{
		public static UnityHashSet<TValue> ToUnityHashSet<TValue>(this ISet<TValue> source)
			=> new (source);
	}
}
