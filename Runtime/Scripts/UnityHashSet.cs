using System;
using System.Collections.Generic;
using UnityEngine;


namespace Valhalla.Serialization
{
	[Serializable]
	public class UnityHashSet<TValue> : HashSet<TValue>, ISerializationCallbackReceiver
	{
		[SerializeField, HideInInspector]
		private List<TValue> _values = new();
		
		
		public UnityHashSet() : base()
		{
			
		}
		
		
		public UnityHashSet(IEnumerable<TValue> source) : base(source)
		{
			
		}
		
		
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			Clear();
			
			if (_values != null && _values.Count > 0)
				UnionWith(_values);
		}


		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_values == null)
				_values = new ();
			else
				_values.Clear();
			
			if (Count > 0)
				_values.AddRange(this);
		}
	}
}
