## About

Contains a generic dictionaty implementations, that can be serialized by Unity.

`UnityDictionary` is an inheritor of `Dictionary<, >`, that implements `ISerializationCallbackReceiver` interface.

Note, that `ISerializationCallbackReceiver` handles only serialization, so in terms of optimization `UnityDictionary` as fast, as basic `Dictionary`.

## Usage

### Definition and serialization
```C#
using Valhalla.Serialization;

...

[SerializeField]
private UnityDictionary<int, Transform> _myUnityDictionary = new ();
```

### Conversion [ Dictionary -> UnityDictionary ]
```C#
using OpenUnitySolutions.Serialization;
using OpenUnitySolutions.Serialization.Extensions;

...

Dictionary<int, int> myInputDefaultDictionary = new ();
myInputDefaultDictionary.Add(1, 100);
myInputDefaultDictionary.Add(2, 200);

UnityDictionary<int, int> myOutputUnityDictionary = myInputDefaultDictionary.ToUnityDictionary();
```

### Conversion [ UnityDictionary -> Dictionary ]
```C#
using OpenUnitySolutions.Serialization;

...

UnityDictionary<int, int> myInputUnityDictionary = new ();
myInputUnityDictionary.Add(1, 100);
myInputUnityDictionary.Add(2, 200);

Dictionary<int, int> myOutputDefaultDictionary = (Dictionary<int, int>) myInputUnityDictionary;
```


### TODO
- [ ] Readme about value drawers
