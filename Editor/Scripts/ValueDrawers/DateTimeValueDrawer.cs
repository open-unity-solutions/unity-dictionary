using System;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;


namespace Valhalla.Serialization.Editor.ValueDrawers
{
	public class DateTimeValueDrawer : OdinValueDrawer<DateTime>
	{
		protected override void DrawPropertyLayout(GUIContent label)
		{
			Rect rect = EditorGUILayout.GetControlRect();

			if (label != null)
			{
				rect = EditorGUI.PrefixLabel(rect, label);
			}

			DateTime target = this.ValueEntry.SmartValue;
			GUIHelper.PushLabelWidth(20);
			EditorGUI.LabelField(rect.AlignLeft(rect.width), target.ToString("yyyy.MM.dd hh:mm:ss"));
			GUIHelper.PopLabelWidth();

			// TODO this.ValueEntry.SmartValue = target;
		}
	}
}
