## [1.4.1] – 2023-07-11

### Removed
- `KeyAssetDictionary` removed


## [1.4.0] – 2023-07-05

### Added
- `KeyAssetDictionary` for key-based dictionaries
- `UnityDictionary` now can be initialized with comparer

### Changed
- CI/CD updated
- License year updated


## [1.3.0]

### Added
- `UnityHashSet` added for Unity serialization of `HashSet`

### Fixed
- Package name fixed


## [1.2.0]

### Changed
- Rebranded from "Open Unity Solutions" to "Valhalla" (breaking change, affects namespaces and package domain)
- Slightly refactored changelog
- Name of packaged changed from "Unity Dictionary" to "Serialization"


## [1.1.2]

### Fixed
- Editor scripts excluded from build


## [1.1.1]

### Added
- Read-only drawer for `DateTime`


## [1.1.0]

### Added
- Construction from existed `IDictionary` added
- `.ToUnityDictionary()` extension method added to `IDictionary` for convenience in LINQ


## [1.0.1]

### Changed
- Readme updated 


## [1.0.0]

### Changed
- Package created
